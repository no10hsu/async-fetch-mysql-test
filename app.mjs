import Express from 'express';
import Path from 'path';
import CookieParser from 'cookie-parser';
import ReactView from 'express-react-views';

import createError from 'http-errors';
import logger from 'morgan';
import indexRouter from './app/routes/index';

const __dirname = (process.platform === 'win32') ? 
  Path.dirname(new URL(import.meta.url).pathname).slice(1) :
  Path.dirname(new URL(import.meta.url).pathname);

let app = Express();

app.engine('jsx', ReactView.createEngine());
app.set('view engine', 'jsx');
app.set('views', Path.join(__dirname, 'resources', 'views'));

app.use(logger('dev'));

app.use(logger('dev'));
app.use(Express.json());
app.use(Express.urlencoded({ extended: false }));
app.use(CookieParser());
app.use(Express.static(Path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

export default app;