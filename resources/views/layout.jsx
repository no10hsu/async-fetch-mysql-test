import React from 'react';

export default class Layout extends React.Component {
    render () {
        return(
            <html>
                <head>
                    <title>{this.props.title}</title>
                    <link rel="stylesheet" href="src/css/style.css"/>
                </head>
                <body>
                    {this.props.children}
                </body>
            </html>
        );
    }
}