import React from 'react';
import DefaultLayout from './layout';

export default class IndexLayout extends React.Component {
    render() {
        let title = this.props.title;
        return(
            <DefaultLayout title={title}>
                <h1>{title}</h1>
                <p>Welcome to {title}</p>
                <script type="text/javascript" src="src/js/fetchData.js"></script>
            </DefaultLayout>
        );
    }
}