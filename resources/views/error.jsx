import React from 'react';
import DefaultLayout from './layout';

export default class ErrorLayout extends React.Component {
    render() {
        return(
            <DefaultLayout>
                <h1>{this.props.message}</h1>
                <h2>{this.props.error.status}</h2>
                <pre dangerouslySetInnerHTML={{__html: this.props.error.stack}}/>
            </DefaultLayout>
        )
    }

    constructor(props) {
        super(props);
    }
}