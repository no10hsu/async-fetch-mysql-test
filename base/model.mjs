import mysql from 'mariadb';
import config from 'config';

const DBCFG = config.get('database');

export default class BaseModel {

  async createConnection() {
    try {
      return await mysql.createConnection({
        host: DBCFG.host,
        port: DBCFG.port,
        user: DBCFG.user,
        password: DBCFG.pass,
        database: DBCFG.db
      });
    } catch (error) {
      throw error;
    }
  }

  constructor() {
    this.db = this.createConnection();
  }

}