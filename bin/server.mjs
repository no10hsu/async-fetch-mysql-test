import Debug from 'debug';
import HTTP from 'http';
import App from '../app';

import config from 'config';

const PORT = config.get('port');

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

let debug = Debug('asyncfetch:server');

App.set('port', PORT);

var server = HTTP.createServer(App);

server.listen(PORT);
server.on('error', onError);
server.on('listening', onListening);