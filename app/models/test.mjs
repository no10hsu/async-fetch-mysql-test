import BaseModel from '../../base/model'

export default class TestModel extends BaseModel {

  async getAllRecord() {
    let connection = await this.db;
    return connection.query('select * from test');
  }

  constructor() {
    super();
  }

}