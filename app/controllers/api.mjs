import BaseController from '../../base/controller';
import TestModel from '../models/test';

export default class APIController extends BaseController {

  async index(req, res, next) {
    let model = new TestModel();
    let result = await model.getAllRecord();
    res.json(result);
  }

  constructor() {
    super();
  }

}